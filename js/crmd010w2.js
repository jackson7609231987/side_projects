window.encodeURIComponent = function (value) {
    var map = {
        ' ': '%20',
        '#': '%23',
        '%': '%25',
        '&': '%26',
        '+': '%2B',
        '/': '%2F',
        '\'': '\'\'', // double for wio informix sql insert
        '=': '%3D',
        '?': '%3F',
        '\r': '%0D',
        '\n': '%0A'
    };

    return value.replace(/[ #%&+/=''?\r\n]/g, function (c) {
        return map[c];
    });
};

(function () {
    window.notJSON = function (data) {
        try {
            $.parseJSON(data);
            return false;
        } catch (err) {
            return true;
        }
    }
})();



$.ajaxSetup({
    cache: false
});

var domain = 'http://eorder.howtobe.com.tw/scmdb/login/crmd010w_api.html?';



var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function myBrowser() {
    //取得瀏覽器的userAgent字串
    var userAgent = navigator.userAgent;
    var isOpera = userAgent.indexOf("Opera") > -1;
    //判斷是否Opera瀏覽器
    if (isOpera) {
        return "Opera"
    };
    //判斷是否Firefox瀏覽器
    if (userAgent.indexOf("Firefox") > -1) {
        return "FF";
    };
    //判斷是否Chrome瀏覽器
    if (userAgent.indexOf("Chrome") > -1) {
        return "Chrome";
    };
    //判斷是否Safari瀏覽器
    if (userAgent.indexOf("Safari") > -1) {
        return "Safari";
    };
    //判斷是否IE瀏覽器 
    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
        return "IE";
    };
};


$(document).ready(function () {


    var mb = myBrowser();
    if ("IE" == mb) {
        console.log("IE");
    };
    if ("FF" == mb) {
        console.log("Firefox");
    };
    if ("Chrome" == mb) {
        console.log("Chrome");
    };
    if ("Opera" == mb) {
        console.log("Opera");
    };
    if ("Safari" == mb) {
        console.log("Safari");
    };



    $("#test3").click(function () {
        Swal.fire({
            html: '<h3 style=color:red;>梯廂清潔<br>非崇友業務範圍，<br>請聯絡管委會處理，<br>感謝。</h3>',
            showConfirmButton: false,
            timer: 2000
        });
    });

    $("#test6").click(function () {
        Swal.fire({
            html: '<h3 style=color:red;>刷卡機異常<br>非崇友業務範圍，<br>請聯絡管委會處理，<br>感謝。</h3>',
            showConfirmButton: false,
            timer: 2000
        });
    });


    Swal.fire({
        // title: data,
        html: '<center><div class="spin" data-spin style="display: inline"></div></center>',
        showConfirmButton: false,
        background: 'url(img/mem/true3.jpg)',
    })

    var update_door = function () {
        if ($("#test4").is(":checked")) {
            $('#err_f_door').attr('disabled', false);
        } else {
            $('#err_f_door').attr('disabled', true);
        }
    };

    $(update_door);
    $("#test4").change(update_door);


    var update_button = function () {
        if ($("#test5").is(":checked")) {
            $('#err_f_button').attr('disabled', false);
        } else {
            $('#err_f_button').attr('disabled', true);
        }
    };

    $(update_button);
    $("#test5").change(update_button);



    $.getJSON("https://ipv4.jsonip.com/?callback=?", function (data) {
        $("#apply_ip").val(data.ip);
    });

    var item = getUrlParameter("item");
    var ct_no = getUrlParameter("ct_no");
    var elev_no = getUrlParameter("elev_no");

    // var id = getUrlParameter("x");
    // var str = String.format('x={0}', id);
    var str = String.format('item={0}&ct_no={1}&elev_no={2}', item, ct_no, elev_no);
    var url = domain + 't=option&';
    url = String.format('{0}{1}', url, str);
    $.get(url).done(function (data) {
        if (notJSON(data)) {
            return;
        }
        var json = $.parseJSON(data);

        var item = json.item;
        var ct_no = json.ct_no;
        var elev_no = json.elev_no;



        if (item && ct_no && elev_no) {
            $("#report_tel").html(json.report_tel);
            $('input:checkbox:checked[name="err_code"]').prop("checked", false);
            // $('#test4,#test5').prop("checked", false);
            $('#err_f_door,#err_f_button').attr('disabled', true);
            $('#not_com_msg').html("");
            $('#reflection').prop('disabled', false);
            if (json.data) {
                // var json_length = Object.keys(json.data).length
                // var i;
                // for (i = 0; i < json_length; i++) {
                //     console.log(json.data[i]);
                //     $('#test' + json.data[i]).attr("disabled", true);
                //     $('#err_code_' + json.data[i]).attr('style', 'color:gray');
                // };
                $.each(json.data, function (key, value) {
                    if (value == 4) {
                        $('#err_f_door').attr('disabled', true);
                        $('#test' + value).prop("checked", false);
                        $('#test' + value).attr("disabled", true);
                        $('#err_code_' + value).attr('style', 'color:gray');
                    } else if (value == 5) {
                        $('#err_f_button').attr('disabled', true);
                        $('#test' + value).prop("checked", false);
                        $('#test' + value).attr("disabled", true);
                        $('#err_code_' + value).attr('style', 'color:gray');
                    } else {
                        // $('#err_f_door').prop('disabled', false);
                        // $('#err_f_button').prop('disabled', false);
                        $('#test' + value).prop("checked", false);
                        $('#test' + value).attr("disabled", true);
                        $('#err_code_' + value).attr('style', 'color:gray');
                    };
                });
            };
            //  else {
            //      alert('err_code Failed!');
            // };
            if (json.err_f) {
                $.each(json.err_f, function (key, value) {
                    $('#err_f_door,#err_f_button').append($("<option></option>").attr("value", value).text(value + "樓"));
                });
            }

            Swal.fire({
                title: '電梯報馬仔通知',
                html: '<div align="center"><div><img  src="http://eorder.howtobe.com.tw/crmd010w/css/images/horse.gif"></div>是否需要回報處理資訊 ? <br>請於下方選擇<br><span style=color:#3085d6;>是(請填姓名/電話)</span>或<span style=color:red;>否</span>，<br>感謝您的配合。</div>',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '是',
                cancelButtonText: '否'
            }).then(function (result) {
                if (result.value) {
                    Swal.fire({
                        title: '親愛的客戶您好:',
                        html: '<div align="center"><span style="font-size:16px;">為妥善處理您反應之事項，本公司需要蒐集您的個人資料，以便向您進一步詢問詳細情形，或回覆說明、處理結果，同時為落實法令遵循之規定告知您：您所留之姓名，電話等個資，只用來詢問您所反映之問題細節，或回覆處理之進度，無其他用途。<br>我已閱讀並同意個資告知函</span></div>',
                        showConfirmButton: true,
                        confirmButtonText: '我同意',
                    });
                    $('#apply_name,#apply_tel,#apply_mail').attr('disabled', false);
                    $("#apply_mail").val(" ");
                    $("#reflection").click(function () {
                        var apply_name = $("#apply_name").val();
                        var apply_tel = $("#apply_tel").val();
                        var test4 = $('#test4').prop('checked');
                        var err_f_door = $("#err_f_door").val();
                        var test5 = $('#test5').prop('checked');
                        var err_f_button = $("#err_f_button").val();
                        if (!apply_name) {
                            Swal.fire({
                                html: '<h3 style=color:red;>名字未填</h3>',
                                showConfirmButton: false,
                                timer: 2000
                            });
                        } else if (!apply_tel) {
                            Swal.fire({
                                html: '<h3 style=color:red;>電話未填</h3>',
                                showConfirmButton: false,
                                timer: 2000
                            });
                        } else if (test4 && !err_f_door) {
                            Swal.fire({
                                html: '<h3 style=color:red;>門開關異常樓層未選</h3>',
                                showConfirmButton: false,
                                timer: 2000
                            });
                            $("em").html("");
                        } else if (test5 && !err_f_button) {
                            Swal.fire({
                                html: '<h3 style=color:red;>按鈕異常樓層未選</h3>',
                                showConfirmButton: false,
                                timer: 2000
                            });
                            $("em").html("");
                        }
                    });

                } else {
                    $("#apply_name,#apply_tel,#apply_mail").val(" ");
                    $('#apply_name,#apply_tel,#apply_mail').attr('disabled', true);
                    $('#no_agree').hide();
                    // $('#apply_name,#apply_tel,#apply_mail').attr('type', "hidden");
                    $("#reflection").click(function () {
                        var test4 = $('#test4').prop('checked');
                        var err_f_door = $("#err_f_door").val();
                        var test5 = $('#test5').prop('checked');
                        var err_f_button = $("#err_f_button").val();
                        if (test4 && !err_f_door) {
                            Swal.fire({
                                html: '<h3 style=color:red;>門開關異常樓層未選</h3>',
                                showConfirmButton: false,
                                timer: 2000
                            });

                        } else if (test5 && !err_f_button) {
                            Swal.fire({
                                html: '<h3 style=color:red;>按鈕異常樓層未選</h3>',
                                showConfirmButton: false,
                                timer: 2000
                            });

                        } else {
                            $("em").html("");
                        }

                    });
                }
            })

            // Swal.fire({
            //     // title: data,
            //     html: '<h1 style=color:#F5F5DC;>讀取中...</h1>',
            //     showConfirmButton: false,
            //     background: 'url(img/mem/true2.jpg)',
            //     timer: 3000,
            // })
            setTimeout(function () {
                $('#item').html(json.item);
                $('#ct_no').html(json.ct_no);
                $('#elev_no').html(json.elev_no);
                $('#buld_name').html(json.buld_name);
                // $('#adress').html(json.adress);
                $('#not_check_msg').html("灰色字樣為今日已回報項目 , 處理中 . . .");
            }, 3000);

        } else {
            Swal.fire({
                html: '<h3 style=color:red;>該合約目前非原廠保養，若有需要請電聯本公司</h3>'
            });
            $('#not_com_msg').html("該合約目前非原廠保養，若有需要請電聯本公司");
            $('#reflection').prop('disabled', true);
            $('#apply_name,#apply_tel,#apply_mail').attr('disabled', true);
            $('#no_contract').hide();
        };

    });


    $('#apply_desc').val("無");


    $.get(domain + 't=get_err').done(function (data) {
        if (notJSON(data)) {
            return;
        }
        var json = $.parseJSON(data);

        if (json) {
            var json_length = Object.keys(json).length
            // var err_code = Object.values(json)
            var err_code = Object.keys(json).map(function (e) {
                return json[e]
            });
            var i;
            for (i = 1; i <= json_length; i++) {
                $('#err_code_' + i).html(err_code[i - 1]);
            };
        } else {
            alert('err_code Failed!');
        };
    });





    $("#cancel").click(function () {

        Swal.fire({
            // title: data,
            html: '<h1 style=color:#F5F5DC;>清除欄位中...</h1>',
            showConfirmButton: false,
            background: 'url(img/mem/true3.jpg)',
            timer: 1000
        })
        setTimeout(function () {
            $("input[type='text']").val("");
            $("input[type='checkbox']").prop('checked', false);
            $("em").html("");
            $('#apply_desc').val("無");
            $(update_door);
            $("#test4").change(update_door);
            $('#err_f_door').val("");
            $(update_button);
            $("#test5").change(update_button);
            $('#err_f_button').val("");
        }, 1000);

    });

    $("#list").click(function () {

        var item = $("#item").html();
        var ct_no = $("#ct_no").html();
        var elev_no = $("#elev_no").html();

        var str = String.format('&item={0}&ct_no={1}&elev_no={2}', item, ct_no, elev_no);
        var url = domain + 't=option_list&';
        url = String.format('{0}{1}', url, str);
        $.get(url).done(function (data) {
            if (notJSON(data)) {
                return;
            }
            var json = $.parseJSON(data);

            var count = Object.keys(json.data).length;

            var i;
            var j;
            var value = "";

            for (i = 0; i < count; i++) {
                for (j = 0; j < 4; j++) {
                    if (j == 0) {
                        value += '<tr><td class="option_list_time">' + json.data[i][j] + '</td>';
                    } else if (j == 1) {
                        value += '<td class="option_list">' + json.data[i][j] + '</td>';
                    } else if (j == 2) {
                        value += '<td class="option_list_status">' + json.data[i][j] + '</td>';
                    } else {
                        value += '<td class="option_list_finish_date">' + json.data[i][j] + "</td></tr>";
                    };
                }
            };

            // console.log(value);

            if (json) {
                Swal.fire({
                    html: '<center><h3><報馬仔最新回報></h3><table><tr><th>時間</th><th>說明</th><th>進度</th><th>完成</th><tr/>' + value + '</table></center>'
                });
                // alert("[報馬仔前五筆資訊]\n[時間] [說明] [進度] [完成日]\n" + value);
            } else {
                alert('item Failed!');
            };
        });
    });

    // var wow = new WOW({
    //     boxClass: 'wow',
    //     animateClass: 'animated',
    //     offset: 0,
    //     mobile: true,
    //     live: true,
    //     callback: function (box) {},
    //     scrollContainer: null
    // });
    // wow.init();

    $('#form').validate({
        focusInvalid: false,
        errorElement: "em",
        rules: {
            "apply_name": {
                required: true,
                minlength: 2
            },
            "apply_tel": {
                required: true
                // minlength: 9,
                // number: true
            },
            "apply_mail": {
                required: true
                // email: true
            },
            "apply_desc": {
                required: true
            },
            "err_code": {
                required: true
            },
            "err_f_door": {
                required: true
            },
            "err_f_button": {
                required: true
            }
        },
        messages: {
            "apply_name": {
                required: '*必填',
                minlength: '*最少2個字元'
            },
            "apply_tel": {
                required: '*必填'
                // minlength: '*最少9個字元',
                // number: '*請輸入9個數字'
            },
            "apply_mail": {
                required: '*必填'
                // email: '*請輸入正確格式'
            },
            "apply_desc": {
                required: '*最少寫 " 無 "'
            },
            "err_code": {
                required: '*最少選取一筆'
            },
            "err_f_door": {
                required: '*請選取樓層'
            },
            "err_f_button": {
                required: '*請選取樓層'
            }
        },
        errorPlacement: function (error, element) {
            var Error_msg = $(element).data('error');
            if (Error_msg) {
                $(Error_msg).append(error)
            } else {
                error.insertAfter(element);
            };
        },
        submitHandler: function (form, event) {
            $('#reflection').prop('disabled', true);
            event.preventDefault();
            var item = $("#item").html();
            var ct_no = $("#ct_no").html();
            var elev_no = $("#elev_no").html();
            var apply_name = $("#apply_name").val();
            var apply_tel = $("#apply_tel").val();
            var apply_mail = $("#apply_mail").val();
            var apply_ip = $("#apply_ip").val();
            var apply_desc = $("#apply_desc").val();
            // var form_data = $(form).serializeArray();
            var str = String.format('&apply_name={0}&apply_tel={1}&apply_mail={2}&item={3}&ct_no={4}&elev_no={5}&apply_ip={6}&apply_desc={7}', apply_name, apply_tel, apply_mail, item, ct_no, elev_no, apply_ip, apply_desc);
            var url = domain + 't=insert_010&';
            url = String.format('{0}{1}', url, str);

            if (item && ct_no && elev_no) {
                $.get(url).done(function (data) {
                    if (notJSON(data)) {
                        alert("010資料取得失敗請稍候再試")
                        return;
                    }
                    var json_010 = $.parseJSON(data);

                    if (json_010.msg) {
                        Swal.fire({
                            html: '<h3 style=color:red;>' + json_010.msg + '</h3>'
                        });
                    } else {

                        var checkbox = new Array();
                        $('input:checkbox:checked[name="err_code"]').each(function (i) {
                            checkbox[i] = this.value;
                        });




                        var checkbox_length = checkbox.length;
                        var i;
                        var massage = ""

                        if (checkbox_length != 0) {


                            for (i = 1; i <= checkbox_length; i++) {

                                var err_code = checkbox[i - 1];

                                $('#test' + err_code).prop("checked", false);
                                $('#test' + err_code).attr("disabled", true);
                                $('#err_code_' + err_code).attr('style', 'color:gray');

                                if (err_code == 4) {
                                    var err_f = $("#err_f_door").val();
                                    $('#err_f_door').attr("disabled", true);
                                } else if (err_code == 5) {
                                    var err_f = $("#err_f_button").val();
                                    $('#err_f_button').attr("disabled", true);
                                } else {
                                    var err_f = ' ';
                                }

                                $.get(domain + 't=insert_015&sn_no=' + json_010.sn_no + '&apply_item=' + i + '&err_code=' + err_code + '&err_f=' + err_f).done(function (data) {

                                    if (notJSON(data)) {
                                        alert("015資料取得失敗請稍候再試")
                                        return;
                                    }
                                    var json_015 = $.parseJSON(data);
                                    massage += json_015.msg
                                    console.log(massage);
                                });
                            }
                            $.get(domain + 't=insert_mal&sn_no=' + json_010.sn_no + '&item=' + json_010.item + '&ct_no=' + json_010.ct_no + '&elev_no=' + json_010.elev_no).done(function (data) {

                                if (notJSON(data)) {
                                    alert("mal資料取得失敗請稍候再試")
                                    return;
                                }
                                var json_mal = $.parseJSON(data);
                                console.log(json_mal);
                            });

                            Swal.fire({
                                // title: data,
                                html: '<h1 style=color:#F5F5DC;>已送出</h1>',
                                showConfirmButton: false,
                                background: 'url(img/mem/true3.jpg)',
                                timer: 3000
                            })
                            setTimeout(function () {
                                $("input[type='text']").val("");
                                $("input[type='checkbox']").prop('checked', false);
                                $("em").html("");
                                $('#apply_desc').val("無");
                            }, 3000);

                        } else {
                            Swal.fire({
                                // title: data,
                                html: '<h1 style=color:#F5F5DC;>回報選項處理中</h1>',
                                showConfirmButton: false,
                                background: 'url(img/mem/true3.jpg)',
                                timer: 3000
                            })
                        };

                    };
                });
            } else {
                Swal.fire({
                    // title: data,
                    html: '<h2 style=color:#F5F5DC;>請稍待,合約讀取中...</h2>',
                    showConfirmButton: false,
                    background: 'url(img/mem/true3.jpg)',
                    timer: 2000
                });
            };

            $('#reflection').prop('disabled', false);
        }
    });


});