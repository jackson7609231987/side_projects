window.encodeURIComponent = function (value) {
  var map = {
    ' ': '%20',
    '#': '%23',
    '%': '%25',
    '&': '%26',
    '+': '%2B',
    '/': '%2F',
    '\'': '\'\'', // double for wio informix sql insert
    '=': '%3D',
    '?': '%3F',
    '\r': '%0D',
    '\n': '%0A'
  };

  return value.replace(/[ #%&+/=''?\r\n]/g, function (c) {
    return map[c];
  });
};

(function () {
  window.notJSON = function (data) {
    try {
      $.parseJSON(data);
      return false;
    } catch (err) {
      return true;
    }
  }
})();



$.ajaxSetup({
  cache: false
});

var domain_woo = './json/json_woo.html';

var domain_ani = './json/json_ani.html';

var domain_wea = './json/json_wea.html';

var domain_wat = './json/json_wat.html';

var domain_peo = './json/json_peo.html';



var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;
  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
};

function myBrowser() {
  //取得瀏覽器的userAgent字串
  var userAgent = navigator.userAgent;
  var isOpera = userAgent.indexOf("Opera") > -1;
  //判斷是否Opera瀏覽器
  if (isOpera) {
    return "Opera"
  };
  //判斷是否Firefox瀏覽器
  if (userAgent.indexOf("Firefox") > -1) {
    return "FF";
  };
  //判斷是否Chrome瀏覽器
  if (userAgent.indexOf("Chrome") > -1) {
    return "Chrome";
  };
  //判斷是否Safari瀏覽器
  if (userAgent.indexOf("Safari") > -1) {
    return "Safari";
  };
  //判斷是否IE瀏覽器 
  if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
    return "IE";
  };
};


var mb = myBrowser();
if ("IE" == mb) {
  console.log("IE");
};
if ("FF" == mb) {
  console.log("Firefox");
};
if ("Chrome" == mb) {
  console.log("Chrome");
};
if ("Opera" == mb) {
  console.log("Opera");
};
if ("Safari" == mb) {
  console.log("Safari");
};

function star_count() {
  if (typeof (Storage) !== "undefined") {
    if (sessionStorage.clickcount) {
      sessionStorage.clickcount = Number(sessionStorage.clickcount) + 1;
    } else {
      sessionStorage.clickcount = 1;
    }
    var count = sessionStorage.clickcount;
    var i;
    for (i = 1; i <= count; i++) {
      $("#span" + i).addClass("star staryellow");
    }
  }
  console.log(count)
};


$(document).ready(function () {
  $("#second").blur(function () {
    var sec = $("#second").val();
    if (sec <= 0 && sec != "") {
      sec = 0;
      $("#second").val(sec);
    } else if (sec == "") {
      sec = 60;
      $("#second").val(sec);
    }
  });
  //主頁選擇成語編號後跳到圖畫頁面帶入參數
  $("#sort").change(function () {
    $('#item option').detach();
    $('<option>').val("").html("請選擇").appendTo('#item');
    var sort = $("#sort option:selected").val();
    var domain = ""
    if (sort == "植物") {
      domain = domain_woo;
    } else if (sort == "動物") {
      domain = domain_ani;
    } else if (sort == "天氣") {
      domain = domain_wea;
    } else if (sort == "水") {
      domain = domain_wat;
    } else if (sort == "人物") {
      domain = domain_peo;
    }

    $.get(domain).done(function (data) {
      if (notJSON(data)) {
        return;
      }
      var json = $.parseJSON(data);
      $.each(json.data[0], function (item, chinese) {
        var num = item.substr(5.6)
        $('<option>').val(item).html("編號" + num).appendTo('#item');
      })


      $("#log_in").click(function () {
        var str = $("#item option:selected").val();
        var sec = $("#second").val();
        if (sec == "") {
          sec = 60;
        }
        var href = "./idiom.html?&location=" + domain + "&item=" + str + "&sec=" + sec
        Swal.fire({
          // title: data,
          html: '讀取中...<br><img src="images/spin.gif">',
          showConfirmButton: false,
          timer: 3000
        })
        setTimeout(function () {
          window.location.href = href;
        }, 3000);
      });
    })
  })

  $("#confirm").click(function () {
    var ans = $("#answer").val();
    var sec = getUrlParameter("sec");
    var item = getUrlParameter("item");
    var num = item.substr(5.6)
    var location = getUrlParameter("location");
    var url = location;
    $.get(url).done(function (data) {
      if (notJSON(data)) {
        return;
      }
      var json = $.parseJSON(data);

      var item_list = [];
      var his_list = [];
      var sam_list = [];
      var aud_list = [];


      $.each(json.data[0], function (item, chinese) {
        item_list.push(chinese);
      });
      $.each(json.data[3], function (item, chinese) {
        his_list.push(chinese);
      });
      $.each(json.data[4], function (item, chinese) {
        sam_list.push(chinese);
      });
      $.each(json.data[5], function (item, chinese) {
        aud_list.push(chinese);
      });


      if (ans == "") {
        Swal.fire({
          // title: data,
          type: "warning",
          html: '<h1>請勿空白...</h1><br><audio autoplay src="audio/voc/請勿空白.mp3"></audio>',
          showConfirmButton: false,
          timer: 2000
        })
        $("#answer").attr('disabled', false);
      } else if (item == "") {
        Swal.fire({
          // title: data,
          type: "warning",
          html: '<h1>未選擇編號，跳轉中...</h1><br><img src="images/spin.gif"><br><audio autoplay src="audio/voc/跳轉中.mp3"></audio>',
          showConfirmButton: false,
          timer: 3500
        })
        setTimeout(function () {
          var re = "true"
          window.location.href = "./index.html?&re=" + re;
        }, 3500);

      } else if (ans) {
        var num = item.substr(5, 6)
        var chinese = item_list[num - 1];
        var his = his_list[num - 1];
        var sam = sam_list[num - 1];
        var aud = aud_list[num - 1];
        // console.log(aud);
        if (ans == chinese) {
          Swal.fire({
            // title: data,
            type: "success",
            html: "<h1>答對了</h1><br>正解 : " + chinese + "<br> 例句 : " + sam + "<br> 歷史 : " + his + '<br><audio autoplay src="' + aud + '"></audio>',
            showCancelButton: true,
            confirmButtonText: '下一題',
            cancelButtonText: '結束',
          }).then(function (result) {
            if (result.value) {
              // var re = "true"
              // window.location.href = "./index.html?&re=" + re;
              num = Math.floor((Math.random() * 10) + 1)

              var loc_arr = ["./json/json_woo.html", "./json/json_wat.html", "./json/json_wea.html", "./json/json_ani.html", "./json/json_peo.html"]

              var loc = Math.floor((Math.random() * 5))

              window.location.href = "./idiom.html?&location=" + loc_arr[loc] + "&item=item_" + num + "&sec=" + sec
            } else {
              if (typeof (Storage) !== "undefined") {
                if (sessionStorage.clickcount) {
                  sessionStorage.clickcount = Number(sessionStorage.clickcount) + 1;
                } else {
                  sessionStorage.clickcount = 1;
                }
                var count = sessionStorage.clickcount;
                window.location.href = "./success.html?&count=" + count;
              }
            }
          });

        } else {
          Swal.fire({
            // title: data,
            type: "error",
            html: '<h1>答錯了...</h1><br><audio autoplay src="audio/voc/答錯了.mp3"></audio>',
            showConfirmButton: false,
            timer: 3000
          })
        }

      }

    });

  });


  wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: true, // default
    live: true // default
  })
  wow.init();




  $("#tip").click(function () {
    var item = getUrlParameter("item");
    var location = getUrlParameter("location");
    var url = location;
    $.get(url).done(function (data) {
      if (notJSON(data)) {
        return;
      }
      var json = $.parseJSON(data);

      var tip_list = [];

      var tip_rev_list = [];

      // var tip_img_list = [];

      $.each(json.data[1], function (item, tip) {

        tip_list.push(tip);
      });

      $.each(json.data[2], function (item, tip) {

        tip_rev_list.push(tip);
      });

      // $.each(json.data[6], function (item, tip) {

      //   tip_img_list.push(tip);
      // });

      var num = item.substr(5, 5)
      var tip = tip_list[num - 1];
      var rev = tip_rev_list[num - 1];
      // var img = tip_img_list[num - 1];

      Swal.fire({
        // title: data,
        type: "info",
        html: tip + "<br>反義詞 : " + rev + '<br><audio autoplay src="audio/voc/提示來囉!.mp3"></audio>',
        showConfirmButton: true
      })

    });

  });





});