$(document).ready(function () {
  var bColor = ["#000000", "#999999", "#CC66FF", "#FF0000", "#FF9900", "#FFFF00", "#008000", "#00CCFF", "#f4f4f4"],
    current_col = "#000000",
    jq_Cav = $('#cav');

  function initBrush() {
    for (var i = 0, len = bColor.length; i < len; i++) {
      var bk = $('<span class="bk"></span>').css("background-color", bColor[i]).click(function () {
        $(this).siblings().removeClass('active').end().addClass('active');
        current_col = $(this).css('background-color');
      });
      $("#bk").append(bk);
    }
  }

  function initPainter() {
    var cav = document.getElementById('cav'),
      self = this,
      x = 0,
      y = 0,
      ctx = cav.getContext("2d");

    ctx.lineWidth = 10;



    // bind click time
    jq_Cav.on("mousedown", function (e) {
      e.preventDefault();
      ctx.strokeStyle = current_col;
      x = e.offsetX;
      y = e.offsetY;

      // start path
      ctx.beginPath();
      ctx.moveTo(x, y);

      // bind mouse move 
      jq_Cav.on('mousemove', function (e) {
        var nx = e.offsetX,
          ny = e.offsetY;
        ctx.lineTo(x, y);
        ctx.stroke();
        x = nx;
        y = ny;
      });

      // bind mouse end
      jq_Cav.on("mouseup", function (e) {
        $(this).off("mousemove");
      });

      // clear canvas
      $('.btn-clear').on('click', function () {
        ctx.clearRect(0, 0, cav.width, cav.height);
      });
    });
  }
  $(function () {
    initBrush();
    initPainter();

  });

});
